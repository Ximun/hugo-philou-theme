# Hugo Philou Theme

This Hugo theme has been created from the [Resume Hugo Theme](https://themes.gohugo.io/hugo-resume/).

I made it for [Philippe Crespeau](https://philippe-crespeau.fr)'s comedian website. It is perfect for simply display photos and Youtube Videos. It will be soon possible to display Peertube Videos.

### Generate Thumbnail command

`for img in static/img/gallery/images/*.jpg;do thumb=$(echo $img | sed 's/\/images\//\/thumb\//g');convert ${img} -thumbnail 500x500^ -gravity center -extent 500x500 -quality 75 ${thumb};done;`
