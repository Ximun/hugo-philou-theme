(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#sideNav'
  });



  $(function () {
    $('[data-toggle="tooltip"]').tooltip()
  })

  // auto youtube iframe height resize
  $(function () {
      $('#youtube-demo').height($('#youtube-demo').width() / 1.78)
  })

  // auto photo height resize
  $(function () {
      $('.photo-element').height($('.photo-element').width())
  })

  // Iterate AOS delay on about <p>
  $(function () {
      $('#about p').each(function(e) {
          $(this).attr('data-aos-delay', 200*(e+1))
          console.log(this, e+1)
      })
  })

  // Initialize Maghnific-Popup Video elemements
  $(function () {
    $('#videos .video-element').each(function () {
        // $(this).append('<img src="https://img.youtube.com/vi/' + $(this).attr('video-id') + '/maxresdefault.jpg"/>')
        var id = $(this).attr('video-id')
        $(this).height($(this).width() / 1.78)
        $(this).css('background-image', 'url(https://img.youtube.com/vi/' + id + '/hqdefault.jpg)')
        $(this).magnificPopup({
            items: [
                {
                    src: 'https://www.youtube.com/watch?v=' + id,
                    type: 'iframe'
                }
            ]
        })
    })
})

// Initialize Magnific-Popup Photos elements
$(function () {
    $('#photos .photo-element').each(function () {
        $(this).height($(this).width())
    })
    $('#photos #photos-content').magnificPopup({
        delegate: 'a',
        type: 'image',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1]
        }
    })
})


})(jQuery); // End of use strict
